# Presets:

**Purpose:** Preset all of the environment variables, settings, so that tag-related styles can be made efficiently and with ease
**Preferred load order:** Typeface, vars, mixins, keyframes
**Global load order:** First