class ApplicationController < ActionController::Base
  include Pundit

  protect_from_forgery

  before_action :http_basic_auth
  before_action :configure_permitted_parameters, if: :devise_controller?

  def index
    @current_user = !current_user.nil?
  end

  def contact; end

  def about; end

  private

  def http_basic_auth
    return unless ENV["HTTP_BASIC_AUTH_ENABLED"] == "true"

    authenticate_or_request_with_http_basic("Username and Password please") do |username, password|
      username == ENV["HTTP_BASIC_AUTH_USERNAME"] && password == ENV["HTTP_BASIC_AUTH_PASSWORD"]
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |u|
      u.permit(:username, :email, :password, :password_confirmation, :remember_me)
    end
    devise_parameter_sanitizer.permit(:sign_in) do |u|
      u.permit(:login, :username, :email, :password, :remember_me)
    end
    devise_parameter_sanitizer.permit(:account_update) do |u|
      u.permit(:username, :email, :password, :password_confirmation, :current_password)
    end
  end
end
