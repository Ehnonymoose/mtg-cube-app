class FollowsController < ApplicationController
  def index
    @follows = []
    @follows = Follow.where(source_id: current_user.id) if current_user
  end

  def show
    set_follow

    respond_to do |format|
      format.html { render :show }
      format.json { render json: @follow.to_json }
    end
  end

  def create
    return unless valid_follow?
    follow = Follow.new(source_id: current_user.id, destination_id: params[:destination_id])
    return follow if follow.save
  end

  def destroy
    set_follow
    unfollowed_username = @follow.username
    @follow.destroy
    redirect_to "/profile", notice: "#{unfollowed_username} unfollowed."
  end

  private

  def set_follow
    return unless current_user
    @follow = (
      if params[:destination_id]
        current_user.follows.where(destination_id: params[:destination_id])
      else
        current_user.follows.find(params[:id])
      end
    )
    authorize @follow
  end

  def follow_params
    params.require(:follow).permit(:id, :destination_id)
  end

  def valid_follow?
    return false if current_user.nil?
    return false if current_user.to_param == params[:destination_id]
    return false if follow_exists?(current_user.id, params[:destination_id])
    true
  end

  def follow_exists?(source_id, destination_id)
    Follow.where(source_id: source_id, destination_id: destination_id).any?
  end
end
