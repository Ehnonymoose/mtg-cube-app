class UsersController < ApplicationController
  def search
    render json: User.search(params[:search] || "", params[:limit] || 10, current_user || nil)
  end

  def show
    set_user
    @current_user = !current_user.nil?

    return redirect_to "/", notice: "Sign in to visit your profile" if !@current_user && params[:username].nil?
    return redirect_to "/", notice: "#{@user.username} has not confirmed their account yet." if @unconfirmed_user

    authorize @user
    set_cubes
    set_decks
    set_cards
    set_follows
  end

  private

  def set_user
    @other_user = true
    @unconfirmed_user = false

    if params[:username]
      @user = User.where(username: params[:username])[0]
      @other_user = false if @user == current_user
    elsif current_user
      @user = current_user
      @other_user = false
    end

    @unconfirmed_user = true if @other_user && !@user&.confirmed?
  end

  def set_cubes
    @cubes = @user.cubes || []
  end

  def set_decks
    @decks = @cubes.any? ? @user&.decks : []
  end

  def set_cards
    @cards = @other_user ? [] : @user&.cards
  end

  def set_follows
    @follows = @other_user ? [] : @user&.follows
  end

  def user_params
    params.require(:user).permit(:id, :username)
  end
end
