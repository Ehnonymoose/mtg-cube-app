import ReactDOM from "react-dom"
import React, { Component, Fragment } from "react"
import $ from "jquery"

export default class UserSearch extends Component {
  state = {
    query: "",
    searchDelay: 100,
    results: null,
    searching: false,
    resultLimit: 10
  }

  search = query => {
    const { searchDelay, resultLimit } = this.state

    if (query !== "") {
      if (this.searchTimeout !== null) clearTimeout(this.searchTimeout)

      this.searchTimeout = setTimeout(() => {
        this.setState({ searching: true })

        $.ajax({
          url: `/users/search?limit=${resultLimit}`,
          method: "GET",
          data: { search: query },
          headers: {
            "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content")
          }
        })
          .done(response => {
            this.setState({
              results: response,
              searching: false
            })
          })
          .fail(error => {
            console.error("Error: ", error)
          })
      }, searchDelay)
    } else {
      this.setState({
        results: null,
        searching: false
      })
    }
  }

  onSearchChange = e => {
    if (this.state.query !== e.target.value) {
      this.setState({
        query: e.target.value
      })

      this.search(e.target.value)
    }
  }

  render() {
    const { query, results, searching } = this.state

    return (
      <section className="user-search__wrap">
        <header className="user-search__header">
          <h3 className="user-search__title title">Find a user</h3>
        </header>

        <form className="form__field-wrap user-search__field-wrap">
          <label className="user-search__field-label form__field-label">
            Enter a username or email address
          </label>
          <input
            type="text"
            placeholder="..."
            className="user-search__field form__field"
            value={query}
            onChange={e => this.onSearchChange(e)}
          />
        </form>

        <SearchResults results={results} searching={searching} />
      </section>
    )
  }
}

const SearchResults = props => {
  const { results, searching } = props
  let ret = null

  if (results !== null) {
    if (results.length > 0) {
      ret = (
        <ul className="list__wrap search-result__list">
          {results.map(result => (
            <Result {...result} key={`user_search_result_${result.id}`} />
          ))}
        </ul>
      )
    } else {
      if (searching) {
        ret = <div className="user-search__searching-placeholder">Searching...</div>
      } else {
        ret = <div className="user-search__no-results no-results">No results</div>
      }
    }
  }

  return <section className="user-search__results-wrap">{ret}</section>
}

class Result extends Component {
  state = {
    followButtonClicked: false,
    following: false
  }

  componentDidMount = () => {
    const { id } = this.props

    $.ajax({
      url: "/follows",
      method: "GET",
      data: {
        destination_id: id
      },
      headers: {
        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content")
      }
    })
      .done(response => {
        this.setState({
          following: true
        })
      })
      .fail(error => {
        console.error("Error: ", error)
      })
  }

  follow = e => {
    const { id } = this.props

    e.preventDefault()
    this.setState({
      followButtonClicked: true
    })

    $.ajax({
      url: "/follows",
      method: "POST",
      data: {
        destination_id: id
      },
      headers: {
        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content")
      }
    })
      .done(response => {
        this.setState({
          followButtonClicked: false,
          following: true
        })
      })
      .fail(error => {
        console.error("Error: ", error)
        this.setState({
          followButtonClicked: false
        })
      })
  }

  unfollow = e => {
    const { id } = this.props

    e.preventDefault()
    this.setState({
      followButtonClicked: true
    })

    $.ajax({
      url: "/follows/destroy",
      method: "DELETE",
      data: {
        destination_id: id
      },
      headers: {
        "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content")
      }
    })
      .done(response => {
        this.setState({
          followButtonClicked: false,
          following: false
        })
      })
      .fail(error => {
        console.error("Error: ", error)
        this.setState({
          followButtonClicked: false
        })
      })
  }

  render() {
    const { username } = this.props
    const { following, followButtonClicked } = this.state
    const userLoggedIn = $("#user_search").attr("data-current-user") == "true"

    return (
      <li className="list__item user-search__result search-result__wrapper">
        <header className="search-result__header">
          <h4 className="search-result__header">
            <a href={`/profile/${username}`} className="search-result__link">
              User: {username}
            </a>
          </h4>
        </header>
        <div className="search-result__info">
          <a href={`/profile/${username}`} className="search-result__button btn">
            View profile
          </a>
          {userLoggedIn &&
            (following ? (
              followButtonClicked ? (
                <div className="search-result__button btn">Unfollowing...</div>
              ) : (
                <button className="search-result__button btn" onClick={this.unfollow}>
                  Unfollow
                </button>
              )
            ) : followButtonClicked ? (
              <div className="search-result__button btn">Following...</div>
            ) : (
              <button className="search-result__button btn" onClick={this.follow}>
                Follow
              </button>
            ))}
        </div>
      </li>
    )
  }
}

ReactDOM.render(<UserSearch />, document.getElementById("user_search"))
