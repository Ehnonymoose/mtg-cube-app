class Cube < ApplicationRecord
  has_many :cards, dependent: :destroy
  has_many :decks, dependent: :destroy

  acts_as_taggable_on :cube_tags
end
