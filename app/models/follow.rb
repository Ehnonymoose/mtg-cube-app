class Follow < ApplicationRecord
  belongs_to :source, class_name: "User", inverse_of: :follows
  belongs_to :destination, class_name: "User", inverse_of: false

  delegate :username, to: :destination
end
