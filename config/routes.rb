Rails.application.routes.draw do
  devise_for :users
  get "/users/search" => "users#search", constraints: { format: "json" }
  get "/profile" => "users#show"
  get "/profile/:username" => "users#show"

  get "/contact" => "application#contact"
  get "/about" => "application#about"

  resources :cubes
  get "/draft/:id" => "cubes#draft"

  resources :decks, only: %i[show edit]

  resources :follows, only: %i[index show create destroy]

  root to: "application#index"
end
