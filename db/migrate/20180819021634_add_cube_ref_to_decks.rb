class AddCubeRefToDecks < ActiveRecord::Migration[5.2]
  def change
    add_reference :decks, :cube, foreign_key: true
  end
end
