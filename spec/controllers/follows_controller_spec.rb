require "rails_helper"
require "devise"

RSpec.describe FollowsController, type: :controller do
  describe "GET #index" do
    before { get :index }

    it "responds with 200 OK" do
      expect(response.status).to eq 200
    end
  end

  describe "POST #create" do
    context "with one user" do
      let(:user1) { FactoryBot.create(:user) }

      before { login_with(user1) }

      it "doesn't follow self" do
        expect do
          post :create, params: { destination_id: user1.to_param }
        end.not_to change(Follow, :count)
      end
    end

    context "with multiple users" do
      let(:user1) { FactoryBot.create(:user) }
      let(:user2) { FactoryBot.create(:user, :with_unique_params) }

      before { login_with(user1) }

      it "creates new follow" do
        expect do
          post :create, params: { destination_id: user2.to_param }
        end.to change(Follow, :count).by(1)
      end

      it "doesn't create duplicate follow" do
        post :create, params: { destination_id: user2.to_param }

        expect do
          post :create, params: { destination_id: user2.to_param }
        end.not_to change(Follow, :count)
      end
    end
  end

  describe "GET #show" do
    let(:user1) { FactoryBot.create(:user) }
    let(:user2) { FactoryBot.create(:user, :with_unique_params) }

    before do
      post :create, params: { destination_id: user2.to_param }
    end

    it "responds with 200 OK" do
      get :show, params: { id: user1.to_param }
      expect(response.status).to eq 200
    end

    describe "responds to" do
      it "responds to html by default" do
        get :show, params: { id: user1.to_param }
        expect(response.content_type).to eq "text/html"
      end

      it "responds to custom formats when provided in the params" do
        get :show, params: { id: user1.to_param }, format: :json
        expect(response.content_type).to eq "application/json"
      end
    end
  end

  describe "DELETE #destroy" do
    let(:user1) { FactoryBot.create(:user) }
    let(:user2) { FactoryBot.create(:user, :with_unique_params) }
    let!(:follow) { FactoryBot.create(:follow, source_id: user1.to_param, destination_id: user2.to_param) }

    before { login_with(user1) }

    it "redirects to profile" do
      delete :destroy, params: { id: follow.to_param }
      expect(response).to redirect_to profile_path
    end

    it "deletes follow" do
      expect do
        delete :destroy, params: { id: follow.to_param }
      end.to change(Follow, :count).by(-1)
    end
  end
end
