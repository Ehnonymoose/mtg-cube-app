require "rails_helper"
require "devise"

RSpec.describe UsersController, type: :controller do
  describe "GET #search" do
    context "without a search query" do
      before { get :search }

      it "responds with 200 OK" do
        expect(response.status).to eq 200
      end
    end

    context "with a search query" do
      before { get :search, params: { query: "e" } }

      it "responds with 200 OK" do
        expect(response.status).to eq 200
      end
    end
  end

  describe "GET #show" do
    let!(:user) { FactoryBot.create(:user) }

    context "when visiting someone else's profile" do
      context "before the user has confirmed their profile" do
        it "responds with 302 Redirect" do
          get :show, params: { username: user.username }
          expect(response.status).to eq 302
        end
      end

      context "after the user has confirmed their profile" do
        before { user.confirm }

        it "responds with 200 OK" do
          get :show, params: { username: user.username }
          expect(response.status).to eq 200
        end
      end
    end

    context "when visiting your own profile" do
      before { login_with(user) }

      it "responds with 200 OK" do
        get :show
        expect(response.status).to eq 200
      end
    end
  end
end
