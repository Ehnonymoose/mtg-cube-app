FactoryBot.define do
  factory :follow do
    source { FactoryBot.build(:user) }
    destination { FactoryBot.build(:user, :with_unique_params) }
  end
end
