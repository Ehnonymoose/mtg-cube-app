FactoryBot.define do
  factory :user do
    username { "placeholder" }
    email { "test@email.com" }
    password { "Placeholder.1" }
    password_confirmation { "Placeholder.1" }

    trait :draft do
      # default
    end

    trait :with_less_than_8_characters do
      username { "toosmal" }
    end

    trait :with_more_than_24_characters do
      username { "test.test.test.test.12345" }
    end

    trait :with_between_8_and_24_characters do
      username { "validusername" }
    end

    trait :with_an_invalid_symbol do
      username { "not?valid" }
    end

    trait :with_a_number do
      username { "validuser2" }
    end

    trait :with_all_valid_characters do
      username { "valid.user-number_3" }
    end

    trait :example_existing_username do
      username { "invaliduser" }
    end

    trait :check_existing_username do
      username { "invaliduser" }
    end

    trait :with_unique_username do
      username { "i.am.unique.1" }
    end

    trait :with_empty_email do
      email { "" }
    end

    trait :with_cased_unique_email do
      email { "TEST@email.com" }
    end

    trait :with_short_password do
      password { "pass" }
    end

    trait :with_long_password do
      password { "passwpasswpasswpasswpassw" }
    end

    trait :without_cap do
      password { "password1." }
    end

    trait :without_lower do
      password { "PASSWORD1." }
    end

    trait :without_num do
      password { "password." }
    end

    trait :without_char do
      password { "password1" }
    end

    trait :with_unique_params do
      username { "validusername2" }
      email { Faker::Internet.email }
    end
  end
end
