require "rails_helper"

RSpec.describe "follows/_index", type: :view do
  context "when follows don't exist" do
    before do
      render partial: "follows/index", locals: { follows: [] }
    end

    it "recognizes there are no follows" do
      expect(rendered).to have_content "You don't follow any users yet"
    end

    it "links home" do
      expect(rendered).to have_link "find a friend", href: "/"
    end
  end

  context "when follows exist" do
    let(:follow) { FactoryBot.create(:follow) }

    before do
      render partial: "follows/index", locals: { follows: [follow] }
    end

    it "contains heading" do
      expect(rendered).to have_content "Follows:"
    end

    it "has list for follows" do
      expect(rendered).to have_css ".user-profile__follows-list"
    end

    it "lists follow" do
      expect(rendered).to have_content follow.username
    end

    it "links to profile of followed person" do
      expect(rendered).to have_link follow.username, href: "/profile/#{follow.username}"
    end
  end
end
